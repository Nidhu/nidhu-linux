# Install script for directory: /mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/libcalamares

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "0")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

# Set default install directory permissions.
if(NOT DEFINED CMAKE_OBJDUMP)
  set(CMAKE_OBJDUMP "/usr/bin/objdump")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xCALAMARESx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libcalamares.so.3.2.34-3" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libcalamares.so.3.2.34-3")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libcalamares.so.3.2.34-3"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE SHARED_LIBRARY FILES "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/build/libcalamares.so.3.2.34-3")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libcalamares.so.3.2.34-3" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libcalamares.so.3.2.34-3")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libcalamares.so.3.2.34-3"
         OLD_RPATH "/usr/lib/libpython3.9.so:"
         NEW_RPATH "")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libcalamares.so.3.2.34-3")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xCALAMARESx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libcalamares.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libcalamares.so")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libcalamares.so"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE SHARED_LIBRARY FILES "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/build/libcalamares.so")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libcalamares.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libcalamares.so")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libcalamares.so"
         OLD_RPATH "/usr/lib/libpython3.9.so:"
         NEW_RPATH "")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libcalamares.so")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xCALAMARESx" OR NOT CMAKE_INSTALL_COMPONENT)
  
    file( MAKE_DIRECTORY "$ENV{DESTDIR}//usr/lib/calamares" )
    execute_process( COMMAND "/usr/bin/cmake" -E create_symlink ../libcalamares.so.3.2.34-3 libcalamares.so WORKING_DIRECTORY "$ENV{DESTDIR}//usr/lib/calamares" )

endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xCALAMARESx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/libcalamares" TYPE FILE FILES
    "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/build/src/libcalamares/CalamaresConfig.h"
    "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/libcalamares/CppJob.h"
    "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/libcalamares/DllMacro.h"
    "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/libcalamares/GlobalStorage.h"
    "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/libcalamares/Job.h"
    "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/libcalamares/JobExample.h"
    "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/libcalamares/JobQueue.h"
    "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/libcalamares/ProcessJob.h"
    "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/libcalamares/PythonHelper.h"
    "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/libcalamares/PythonJob.h"
    "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/libcalamares/PythonJobApi.h"
    "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/libcalamares/Settings.h"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xCALAMARESx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/libcalamares/geoip" TYPE FILE FILES
    "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/libcalamares/geoip/GeoIPFixed.h"
    "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/libcalamares/geoip/GeoIPJSON.h"
    "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/libcalamares/geoip/GeoIPTests.h"
    "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/libcalamares/geoip/GeoIPXML.h"
    "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/libcalamares/geoip/Handler.h"
    "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/libcalamares/geoip/Interface.h"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xCALAMARESx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/libcalamares/locale" TYPE FILE FILES
    "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/libcalamares/locale/Global.h"
    "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/libcalamares/locale/Label.h"
    "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/libcalamares/locale/LabelModel.h"
    "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/libcalamares/locale/Lookup.h"
    "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/libcalamares/locale/TimeZone.h"
    "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/libcalamares/locale/TranslatableConfiguration.h"
    "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/libcalamares/locale/TranslatableString.h"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xCALAMARESx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/libcalamares/modulesystem" TYPE FILE FILES
    "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/libcalamares/modulesystem/Actions.h"
    "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/libcalamares/modulesystem/Descriptor.h"
    "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/libcalamares/modulesystem/InstanceKey.h"
    "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/libcalamares/modulesystem/Module.h"
    "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/libcalamares/modulesystem/Requirement.h"
    "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/libcalamares/modulesystem/RequirementsChecker.h"
    "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/libcalamares/modulesystem/RequirementsModel.h"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xCALAMARESx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/libcalamares/network" TYPE FILE FILES
    "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/libcalamares/network/Manager.h"
    "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/libcalamares/network/Tests.h"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xCALAMARESx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/libcalamares/partition" TYPE FILE FILES
    "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/libcalamares/partition/FileSystem.h"
    "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/libcalamares/partition/KPMHelper.h"
    "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/libcalamares/partition/KPMManager.h"
    "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/libcalamares/partition/Mount.h"
    "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/libcalamares/partition/PartitionIterator.h"
    "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/libcalamares/partition/PartitionQuery.h"
    "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/libcalamares/partition/PartitionSize.h"
    "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/libcalamares/partition/Sync.h"
    "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/libcalamares/partition/Tests.h"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xCALAMARESx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/libcalamares/utils" TYPE FILE FILES
    "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/libcalamares/utils/BoostPython.h"
    "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/libcalamares/utils/CalamaresUtilsSystem.h"
    "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/libcalamares/utils/CommandList.h"
    "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/libcalamares/utils/Dirs.h"
    "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/libcalamares/utils/Entropy.h"
    "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/libcalamares/utils/Logger.h"
    "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/libcalamares/utils/NamedEnum.h"
    "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/libcalamares/utils/NamedSuffix.h"
    "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/libcalamares/utils/Permissions.h"
    "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/libcalamares/utils/PluginFactory.h"
    "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/libcalamares/utils/RAII.h"
    "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/libcalamares/utils/Retranslator.h"
    "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/libcalamares/utils/String.h"
    "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/libcalamares/utils/Traits.h"
    "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/libcalamares/utils/UMask.h"
    "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/libcalamares/utils/Units.h"
    "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/libcalamares/utils/Variant.h"
    "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/libcalamares/utils/Yaml.h"
    "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/libcalamares/utils/moc-warnings.h"
    )
endif()

