# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.19

# compile CXX with /usr/bin/c++
CXX_DEFINES = -DBUILD_AS_TEST=\"/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/libcalamares\" -DDLLEXPORT_PRO -DKCOREADDONS_LIB -DQT_CORE_LIB -DQT_NETWORK_LIB -DQT_NO_DEBUG -DQT_SHARED -DQT_SHAREDPOINTER_TRACK_POINTERS -DQT_STRICT_ITERATORS -DQT_TESTCASE_BUILDDIR=\"/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/build\" -DQT_TESTLIB_LIB -DQT_XML_LIB -DWITH_KPMCORE331API -DWITH_KPMCORE42API -DWITH_KPMCORE4API

CXX_INCLUDES = -I/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/build/src/libcalamares/libcalamarespartitiontest_autogen/include -I/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/build/src/libcalamares -I/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/libcalamares -I/usr/include/python3.9 -I/usr/include/kpmcore -I/usr/include/libcalamares -isystem /usr/include/qt -isystem /usr/include/qt/QtCore -isystem /usr/lib/qt/mkspecs/linux-g++ -isystem /usr/include/KF5/KCoreAddons -isystem /usr/include/KF5 -isystem /usr/include/qt/QtNetwork -isystem /usr/include/qt/QtXml -isystem /usr/include/qt/QtTest

CXX_FLAGS = -D_FORTIFY_SOURCE=2 -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -Wall -Werror=return-type -Wnon-virtual-dtor -Woverloaded-virtual -fdiagnostics-color=auto -O3 -DNDEBUG -fPIC -std=gnu++17

