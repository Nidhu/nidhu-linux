# CMake generated Testfile for 
# Source directory: /mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src
# Build directory: /mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/build/src
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("libcalamares")
subdirs("libcalamaresui")
subdirs("qml/calamares")
subdirs("calamares")
subdirs("modules")
subdirs("branding")
