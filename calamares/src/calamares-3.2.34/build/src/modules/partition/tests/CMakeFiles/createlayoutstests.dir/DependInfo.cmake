# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/modules/partition/tests/CreateLayoutsTests.cpp" "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/build/src/modules/partition/tests/CMakeFiles/createlayoutstests.dir/CreateLayoutsTests.cpp.o"
  "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/modules/partition/core/DeviceModel.cpp" "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/build/src/modules/partition/tests/CMakeFiles/createlayoutstests.dir/__/core/DeviceModel.cpp.o"
  "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/modules/partition/core/KPMHelpers.cpp" "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/build/src/modules/partition/tests/CMakeFiles/createlayoutstests.dir/__/core/KPMHelpers.cpp.o"
  "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/modules/partition/core/PartUtils.cpp" "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/build/src/modules/partition/tests/CMakeFiles/createlayoutstests.dir/__/core/PartUtils.cpp.o"
  "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/modules/partition/core/PartitionInfo.cpp" "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/build/src/modules/partition/tests/CMakeFiles/createlayoutstests.dir/__/core/PartitionInfo.cpp.o"
  "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/modules/partition/core/PartitionLayout.cpp" "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/build/src/modules/partition/tests/CMakeFiles/createlayoutstests.dir/__/core/PartitionLayout.cpp.o"
  "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/build/src/modules/partition/tests/createlayoutstests_autogen/mocs_compilation.cpp" "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/build/src/modules/partition/tests/CMakeFiles/createlayoutstests.dir/createlayoutstests_autogen/mocs_compilation.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "BUILD_AS_TEST=\"/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/modules/partition/tests\""
  "KCOREADDONS_LIB"
  "QT_CORE_LIB"
  "QT_GUI_LIB"
  "QT_NETWORK_LIB"
  "QT_NO_DEBUG"
  "QT_QMLMODELS_LIB"
  "QT_QML_LIB"
  "QT_QUICKWIDGETS_LIB"
  "QT_QUICK_LIB"
  "QT_SHARED"
  "QT_SHAREDPOINTER_TRACK_POINTERS"
  "QT_STRICT_ITERATORS"
  "QT_SVG_LIB"
  "QT_TESTCASE_BUILDDIR=\"/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/build\""
  "QT_TESTLIB_LIB"
  "QT_WIDGETS_LIB"
  "QT_XML_LIB"
  "WITH_KPMCORE331API"
  "WITH_KPMCORE42API"
  "WITH_KPMCORE4API"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "src/modules/partition/tests/createlayoutstests_autogen/include"
  "../src/libcalamares"
  "src/libcalamares"
  "../src/libcalamaresui"
  "../"
  "/usr/include/kpmcore"
  "src/libcalamaresui"
  "../src/modules/partition/tests/.."
  "../src/modules/partition/tests"
  "src/modules/partition/tests"
  "/usr/include/libcalamares"
  "/usr/include/qt"
  "/usr/include/qt/QtGui"
  "/usr/include/qt/QtCore"
  "/usr/lib/qt/mkspecs/linux-g++"
  "/usr/include/KF5/KCoreAddons"
  "/usr/include/KF5"
  "/usr/include/qt/QtNetwork"
  "/usr/include/qt/QtXml"
  "/usr/include/qt/QtWidgets"
  "/usr/include/qt/QtSvg"
  "/usr/include/qt/QtQuickWidgets"
  "/usr/include/qt/QtQuick"
  "/usr/include/qt/QtQmlModels"
  "/usr/include/qt/QtQml"
  "/usr/include/qt/QtTest"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/build/src/libcalamaresui/CMakeFiles/calamaresui.dir/DependInfo.cmake"
  "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/build/src/libcalamares/CMakeFiles/calamares.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
