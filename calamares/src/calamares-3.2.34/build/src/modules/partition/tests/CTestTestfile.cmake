# CMake generated Testfile for 
# Source directory: /mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/modules/partition/tests
# Build directory: /mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/build/src/modules/partition/tests
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(partitionjobtests "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/build/partitionjobtests")
set_tests_properties(partitionjobtests PROPERTIES  ENVIRONMENT "QT_PLUGIN_PATH=/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/build:" _BACKTRACE_TRIPLES "/usr/share/ECM/modules/ECMAddTests.cmake;93;add_test;/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/CMakeModules/CalamaresAddTest.cmake;34;ecm_add_test;/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/modules/partition/tests/CMakeLists.txt;17;calamares_add_test;/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/modules/partition/tests/CMakeLists.txt;0;")
add_test(clearmountsjobtests "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/build/clearmountsjobtests")
set_tests_properties(clearmountsjobtests PROPERTIES  ENVIRONMENT "QT_PLUGIN_PATH=/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/build:" _BACKTRACE_TRIPLES "/usr/share/ECM/modules/ECMAddTests.cmake;93;add_test;/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/CMakeModules/CalamaresAddTest.cmake;34;ecm_add_test;/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/modules/partition/tests/CMakeLists.txt;33;calamares_add_test;/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/modules/partition/tests/CMakeLists.txt;0;")
add_test(createlayoutstests "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/build/createlayoutstests")
set_tests_properties(createlayoutstests PROPERTIES  ENVIRONMENT "QT_PLUGIN_PATH=/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/build:" _BACKTRACE_TRIPLES "/usr/share/ECM/modules/ECMAddTests.cmake;93;add_test;/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/CMakeModules/CalamaresAddTest.cmake;34;ecm_add_test;/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/modules/partition/tests/CMakeLists.txt;44;calamares_add_test;/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/modules/partition/tests/CMakeLists.txt;0;")
