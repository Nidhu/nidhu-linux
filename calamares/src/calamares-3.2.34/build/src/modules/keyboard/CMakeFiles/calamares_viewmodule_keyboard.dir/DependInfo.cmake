# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/modules/keyboard/Config.cpp" "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/build/src/modules/keyboard/CMakeFiles/calamares_viewmodule_keyboard.dir/Config.cpp.o"
  "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/modules/keyboard/KeyboardLayoutModel.cpp" "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/build/src/modules/keyboard/CMakeFiles/calamares_viewmodule_keyboard.dir/KeyboardLayoutModel.cpp.o"
  "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/modules/keyboard/KeyboardPage.cpp" "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/build/src/modules/keyboard/CMakeFiles/calamares_viewmodule_keyboard.dir/KeyboardPage.cpp.o"
  "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/modules/keyboard/KeyboardViewStep.cpp" "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/build/src/modules/keyboard/CMakeFiles/calamares_viewmodule_keyboard.dir/KeyboardViewStep.cpp.o"
  "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/modules/keyboard/SetKeyboardLayoutJob.cpp" "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/build/src/modules/keyboard/CMakeFiles/calamares_viewmodule_keyboard.dir/SetKeyboardLayoutJob.cpp.o"
  "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/build/src/modules/keyboard/calamares_viewmodule_keyboard_autogen/EWIEGA46WW/qrc_keyboard.cpp" "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/build/src/modules/keyboard/CMakeFiles/calamares_viewmodule_keyboard.dir/calamares_viewmodule_keyboard_autogen/EWIEGA46WW/qrc_keyboard.cpp.o"
  "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/build/src/modules/keyboard/calamares_viewmodule_keyboard_autogen/mocs_compilation.cpp" "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/build/src/modules/keyboard/CMakeFiles/calamares_viewmodule_keyboard.dir/calamares_viewmodule_keyboard_autogen/mocs_compilation.cpp.o"
  "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/modules/keyboard/keyboardwidget/keyboardglobal.cpp" "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/build/src/modules/keyboard/CMakeFiles/calamares_viewmodule_keyboard.dir/keyboardwidget/keyboardglobal.cpp.o"
  "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/src/modules/keyboard/keyboardwidget/keyboardpreview.cpp" "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/build/src/modules/keyboard/CMakeFiles/calamares_viewmodule_keyboard.dir/keyboardwidget/keyboardpreview.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "KCOREADDONS_LIB"
  "PLUGINDLLEXPORT_PRO"
  "QT_CORE_LIB"
  "QT_GUI_LIB"
  "QT_NETWORK_LIB"
  "QT_NO_DEBUG"
  "QT_QMLMODELS_LIB"
  "QT_QML_LIB"
  "QT_QUICKWIDGETS_LIB"
  "QT_QUICK_LIB"
  "QT_SHARED"
  "QT_SHAREDPOINTER_TRACK_POINTERS"
  "QT_STRICT_ITERATORS"
  "QT_SVG_LIB"
  "QT_WIDGETS_LIB"
  "QT_XML_LIB"
  "calamares_viewmodule_keyboard_EXPORTS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "src/modules/keyboard/calamares_viewmodule_keyboard_autogen/include"
  "../src/libcalamares"
  "src/libcalamares"
  "../src/libcalamaresui"
  "../src/modules/keyboard"
  "src/modules/keyboard"
  "/usr/include/libcalamares"
  "/usr/include/qt"
  "/usr/include/qt/QtCore"
  "/usr/lib/qt/mkspecs/linux-g++"
  "/usr/include/KF5/KCoreAddons"
  "/usr/include/KF5"
  "/usr/include/qt/QtNetwork"
  "/usr/include/qt/QtXml"
  "/usr/include/qt/QtGui"
  "/usr/include/qt/QtWidgets"
  "/usr/include/qt/QtSvg"
  "/usr/include/qt/QtQuickWidgets"
  "/usr/include/qt/QtQuick"
  "/usr/include/qt/QtQmlModels"
  "/usr/include/qt/QtQml"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/build/src/libcalamaresui/CMakeFiles/calamaresui.dir/DependInfo.cmake"
  "/mnt/Nde/nidhu-linux/calamares/src/calamares-3.2.34/build/src/libcalamares/CMakeFiles/calamares.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
