/****************************************************************************
** Meta object code from reading C++ file 'ViewManager.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.15.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../../../../src/libcalamaresui/ViewManager.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'ViewManager.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.15.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Calamares__ViewManager_t {
    QByteArrayData data[42];
    char stringdata0[542];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Calamares__ViewManager_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Calamares__ViewManager_t qt_meta_stringdata_Calamares__ViewManager = {
    {
QT_MOC_LITERAL(0, 0, 22), // "Calamares::ViewManager"
QT_MOC_LITERAL(1, 23, 18), // "currentStepChanged"
QT_MOC_LITERAL(2, 42, 0), // ""
QT_MOC_LITERAL(3, 43, 10), // "ensureSize"
QT_MOC_LITERAL(4, 54, 4), // "size"
QT_MOC_LITERAL(5, 59, 13), // "cancelEnabled"
QT_MOC_LITERAL(6, 73, 7), // "enabled"
QT_MOC_LITERAL(7, 81, 18), // "nextEnabledChanged"
QT_MOC_LITERAL(8, 100, 16), // "nextLabelChanged"
QT_MOC_LITERAL(9, 117, 15), // "nextIconChanged"
QT_MOC_LITERAL(10, 133, 18), // "backEnabledChanged"
QT_MOC_LITERAL(11, 152, 16), // "backLabelChanged"
QT_MOC_LITERAL(12, 169, 15), // "backIconChanged"
QT_MOC_LITERAL(13, 185, 18), // "quitEnabledChanged"
QT_MOC_LITERAL(14, 204, 16), // "quitLabelChanged"
QT_MOC_LITERAL(15, 221, 15), // "quitIconChanged"
QT_MOC_LITERAL(16, 237, 18), // "quitVisibleChanged"
QT_MOC_LITERAL(17, 256, 18), // "quitTooltipChanged"
QT_MOC_LITERAL(18, 275, 4), // "next"
QT_MOC_LITERAL(19, 280, 11), // "nextEnabled"
QT_MOC_LITERAL(20, 292, 9), // "nextLabel"
QT_MOC_LITERAL(21, 302, 8), // "nextIcon"
QT_MOC_LITERAL(22, 311, 4), // "back"
QT_MOC_LITERAL(23, 316, 11), // "backEnabled"
QT_MOC_LITERAL(24, 328, 9), // "backLabel"
QT_MOC_LITERAL(25, 338, 8), // "backIcon"
QT_MOC_LITERAL(26, 347, 4), // "quit"
QT_MOC_LITERAL(27, 352, 11), // "quitEnabled"
QT_MOC_LITERAL(28, 364, 9), // "quitLabel"
QT_MOC_LITERAL(29, 374, 8), // "quitIcon"
QT_MOC_LITERAL(30, 383, 11), // "quitVisible"
QT_MOC_LITERAL(31, 395, 11), // "quitTooltip"
QT_MOC_LITERAL(32, 407, 20), // "onInstallationFailed"
QT_MOC_LITERAL(33, 428, 7), // "message"
QT_MOC_LITERAL(34, 436, 7), // "details"
QT_MOC_LITERAL(35, 444, 12), // "onInitFailed"
QT_MOC_LITERAL(36, 457, 7), // "modules"
QT_MOC_LITERAL(37, 465, 14), // "onInitComplete"
QT_MOC_LITERAL(38, 480, 16), // "updateNextStatus"
QT_MOC_LITERAL(39, 497, 16), // "currentStepIndex"
QT_MOC_LITERAL(40, 514, 10), // "panelSides"
QT_MOC_LITERAL(41, 525, 16) // "Qt::Orientations"

    },
    "Calamares::ViewManager\0currentStepChanged\0"
    "\0ensureSize\0size\0cancelEnabled\0enabled\0"
    "nextEnabledChanged\0nextLabelChanged\0"
    "nextIconChanged\0backEnabledChanged\0"
    "backLabelChanged\0backIconChanged\0"
    "quitEnabledChanged\0quitLabelChanged\0"
    "quitIconChanged\0quitVisibleChanged\0"
    "quitTooltipChanged\0next\0nextEnabled\0"
    "nextLabel\0nextIcon\0back\0backEnabled\0"
    "backLabel\0backIcon\0quit\0quitEnabled\0"
    "quitLabel\0quitIcon\0quitVisible\0"
    "quitTooltip\0onInstallationFailed\0"
    "message\0details\0onInitFailed\0modules\0"
    "onInitComplete\0updateNextStatus\0"
    "currentStepIndex\0panelSides\0"
    "Qt::Orientations"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Calamares__ViewManager[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      32,   14, // methods
      13,  240, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      14,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  174,    2, 0x06 /* Public */,
       3,    1,  175,    2, 0x06 /* Public */,
       5,    1,  178,    2, 0x06 /* Public */,
       7,    1,  181,    2, 0x06 /* Public */,
       8,    1,  184,    2, 0x06 /* Public */,
       9,    1,  187,    2, 0x06 /* Public */,
      10,    1,  190,    2, 0x06 /* Public */,
      11,    1,  193,    2, 0x06 /* Public */,
      12,    1,  196,    2, 0x06 /* Public */,
      13,    1,  199,    2, 0x06 /* Public */,
      14,    1,  202,    2, 0x06 /* Public */,
      15,    1,  205,    2, 0x06 /* Public */,
      16,    1,  208,    2, 0x06 /* Public */,
      17,    1,  211,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      18,    0,  214,    2, 0x0a /* Public */,
      19,    0,  215,    2, 0x0a /* Public */,
      20,    0,  216,    2, 0x0a /* Public */,
      21,    0,  217,    2, 0x0a /* Public */,
      22,    0,  218,    2, 0x0a /* Public */,
      23,    0,  219,    2, 0x0a /* Public */,
      24,    0,  220,    2, 0x0a /* Public */,
      25,    0,  221,    2, 0x0a /* Public */,
      26,    0,  222,    2, 0x0a /* Public */,
      27,    0,  223,    2, 0x0a /* Public */,
      28,    0,  224,    2, 0x0a /* Public */,
      29,    0,  225,    2, 0x0a /* Public */,
      30,    0,  226,    2, 0x0a /* Public */,
      31,    0,  227,    2, 0x0a /* Public */,
      32,    2,  228,    2, 0x0a /* Public */,
      35,    1,  233,    2, 0x0a /* Public */,
      37,    0,  236,    2, 0x0a /* Public */,
      38,    1,  237,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::QSize,    4,
    QMetaType::Void, QMetaType::Bool,    6,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::QString,    2,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Bool,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::Void,
    QMetaType::Bool,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::Void,
    QMetaType::Bool,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::Bool,
    QMetaType::QString,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,   33,   34,
    QMetaType::Void, QMetaType::QStringList,   36,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,    6,

 // properties: name, type, flags
      39, QMetaType::Int, 0x00495801,
      19, QMetaType::Bool, 0x00495801,
      20, QMetaType::QString, 0x00495801,
      21, QMetaType::QString, 0x00495801,
      23, QMetaType::Bool, 0x00495801,
      24, QMetaType::QString, 0x00495801,
      25, QMetaType::QString, 0x00495801,
      27, QMetaType::Bool, 0x00495801,
      28, QMetaType::QString, 0x00495801,
      29, QMetaType::QString, 0x00495801,
      31, QMetaType::QString, 0x00495801,
      30, QMetaType::Bool, 0x00495801,
      40, 0x80000000 | 41, 0x0009510b,

 // properties: notify_signal_id
       0,
       3,
       4,
       5,
       6,
       7,
       8,
       9,
      10,
      11,
      13,
      12,
       0,

       0        // eod
};

void Calamares::ViewManager::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<ViewManager *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->currentStepChanged(); break;
        case 1: _t->ensureSize((*reinterpret_cast< QSize(*)>(_a[1]))); break;
        case 2: _t->cancelEnabled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 3: _t->nextEnabledChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 4: _t->nextLabelChanged((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 5: _t->nextIconChanged((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 6: _t->backEnabledChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 7: _t->backLabelChanged((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 8: _t->backIconChanged((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 9: _t->quitEnabledChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 10: _t->quitLabelChanged((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 11: _t->quitIconChanged((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 12: _t->quitVisibleChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 13: _t->quitTooltipChanged((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 14: _t->next(); break;
        case 15: { bool _r = _t->nextEnabled();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 16: { QString _r = _t->nextLabel();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 17: { QString _r = _t->nextIcon();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 18: _t->back(); break;
        case 19: { bool _r = _t->backEnabled();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 20: { QString _r = _t->backLabel();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 21: { QString _r = _t->backIcon();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 22: _t->quit(); break;
        case 23: { bool _r = _t->quitEnabled();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 24: { QString _r = _t->quitLabel();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 25: { QString _r = _t->quitIcon();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 26: { bool _r = _t->quitVisible();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 27: { QString _r = _t->quitTooltip();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 28: _t->onInstallationFailed((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        case 29: _t->onInitFailed((*reinterpret_cast< const QStringList(*)>(_a[1]))); break;
        case 30: _t->onInitComplete(); break;
        case 31: _t->updateNextStatus((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (ViewManager::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ViewManager::currentStepChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (ViewManager::*)(QSize ) const;
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ViewManager::ensureSize)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (ViewManager::*)(bool ) const;
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ViewManager::cancelEnabled)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (ViewManager::*)(bool ) const;
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ViewManager::nextEnabledChanged)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (ViewManager::*)(QString ) const;
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ViewManager::nextLabelChanged)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (ViewManager::*)(QString ) const;
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ViewManager::nextIconChanged)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (ViewManager::*)(bool ) const;
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ViewManager::backEnabledChanged)) {
                *result = 6;
                return;
            }
        }
        {
            using _t = void (ViewManager::*)(QString ) const;
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ViewManager::backLabelChanged)) {
                *result = 7;
                return;
            }
        }
        {
            using _t = void (ViewManager::*)(QString ) const;
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ViewManager::backIconChanged)) {
                *result = 8;
                return;
            }
        }
        {
            using _t = void (ViewManager::*)(bool ) const;
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ViewManager::quitEnabledChanged)) {
                *result = 9;
                return;
            }
        }
        {
            using _t = void (ViewManager::*)(QString ) const;
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ViewManager::quitLabelChanged)) {
                *result = 10;
                return;
            }
        }
        {
            using _t = void (ViewManager::*)(QString ) const;
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ViewManager::quitIconChanged)) {
                *result = 11;
                return;
            }
        }
        {
            using _t = void (ViewManager::*)(bool ) const;
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ViewManager::quitVisibleChanged)) {
                *result = 12;
                return;
            }
        }
        {
            using _t = void (ViewManager::*)(QString ) const;
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ViewManager::quitTooltipChanged)) {
                *result = 13;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<ViewManager *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< int*>(_v) = _t->currentStepIndex(); break;
        case 1: *reinterpret_cast< bool*>(_v) = _t->nextEnabled(); break;
        case 2: *reinterpret_cast< QString*>(_v) = _t->nextLabel(); break;
        case 3: *reinterpret_cast< QString*>(_v) = _t->nextIcon(); break;
        case 4: *reinterpret_cast< bool*>(_v) = _t->backEnabled(); break;
        case 5: *reinterpret_cast< QString*>(_v) = _t->backLabel(); break;
        case 6: *reinterpret_cast< QString*>(_v) = _t->backIcon(); break;
        case 7: *reinterpret_cast< bool*>(_v) = _t->quitEnabled(); break;
        case 8: *reinterpret_cast< QString*>(_v) = _t->quitLabel(); break;
        case 9: *reinterpret_cast< QString*>(_v) = _t->quitIcon(); break;
        case 10: *reinterpret_cast< QString*>(_v) = _t->quitTooltip(); break;
        case 11: *reinterpret_cast< bool*>(_v) = _t->quitVisible(); break;
        case 12: *reinterpret_cast< Qt::Orientations*>(_v) = _t->panelSides(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<ViewManager *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 12: _t->setPanelSides(*reinterpret_cast< Qt::Orientations*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject Calamares::ViewManager::staticMetaObject = { {
    QMetaObject::SuperData::link<QAbstractListModel::staticMetaObject>(),
    qt_meta_stringdata_Calamares__ViewManager.data,
    qt_meta_data_Calamares__ViewManager,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Calamares::ViewManager::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Calamares::ViewManager::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Calamares__ViewManager.stringdata0))
        return static_cast<void*>(this);
    return QAbstractListModel::qt_metacast(_clname);
}

int Calamares::ViewManager::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QAbstractListModel::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 32)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 32;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 32)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 32;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 13;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 13;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 13;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 13;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 13;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 13;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void Calamares::ViewManager::currentStepChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void Calamares::ViewManager::ensureSize(QSize _t1)const
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(const_cast< Calamares::ViewManager *>(this), &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Calamares::ViewManager::cancelEnabled(bool _t1)const
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(const_cast< Calamares::ViewManager *>(this), &staticMetaObject, 2, _a);
}

// SIGNAL 3
void Calamares::ViewManager::nextEnabledChanged(bool _t1)const
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(const_cast< Calamares::ViewManager *>(this), &staticMetaObject, 3, _a);
}

// SIGNAL 4
void Calamares::ViewManager::nextLabelChanged(QString _t1)const
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(const_cast< Calamares::ViewManager *>(this), &staticMetaObject, 4, _a);
}

// SIGNAL 5
void Calamares::ViewManager::nextIconChanged(QString _t1)const
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(const_cast< Calamares::ViewManager *>(this), &staticMetaObject, 5, _a);
}

// SIGNAL 6
void Calamares::ViewManager::backEnabledChanged(bool _t1)const
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(const_cast< Calamares::ViewManager *>(this), &staticMetaObject, 6, _a);
}

// SIGNAL 7
void Calamares::ViewManager::backLabelChanged(QString _t1)const
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(const_cast< Calamares::ViewManager *>(this), &staticMetaObject, 7, _a);
}

// SIGNAL 8
void Calamares::ViewManager::backIconChanged(QString _t1)const
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(const_cast< Calamares::ViewManager *>(this), &staticMetaObject, 8, _a);
}

// SIGNAL 9
void Calamares::ViewManager::quitEnabledChanged(bool _t1)const
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(const_cast< Calamares::ViewManager *>(this), &staticMetaObject, 9, _a);
}

// SIGNAL 10
void Calamares::ViewManager::quitLabelChanged(QString _t1)const
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(const_cast< Calamares::ViewManager *>(this), &staticMetaObject, 10, _a);
}

// SIGNAL 11
void Calamares::ViewManager::quitIconChanged(QString _t1)const
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(const_cast< Calamares::ViewManager *>(this), &staticMetaObject, 11, _a);
}

// SIGNAL 12
void Calamares::ViewManager::quitVisibleChanged(bool _t1)const
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(const_cast< Calamares::ViewManager *>(this), &staticMetaObject, 12, _a);
}

// SIGNAL 13
void Calamares::ViewManager::quitTooltipChanged(QString _t1)const
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(const_cast< Calamares::ViewManager *>(this), &staticMetaObject, 13, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
