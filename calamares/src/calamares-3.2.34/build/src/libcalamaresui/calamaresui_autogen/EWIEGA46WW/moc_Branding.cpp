/****************************************************************************
** Meta object code from reading C++ file 'Branding.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.15.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../../../../src/libcalamaresui/Branding.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Branding.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.15.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Calamares__Branding_t {
    QByteArrayData data[59];
    char stringdata0[721];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Calamares__Branding_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Calamares__Branding_t qt_meta_stringdata_Calamares__Branding = {
    {
QT_MOC_LITERAL(0, 0, 19), // "Calamares::Branding"
QT_MOC_LITERAL(1, 20, 6), // "string"
QT_MOC_LITERAL(2, 27, 0), // ""
QT_MOC_LITERAL(3, 28, 11), // "StringEntry"
QT_MOC_LITERAL(4, 40, 11), // "stringEntry"
QT_MOC_LITERAL(5, 52, 13), // "versionedName"
QT_MOC_LITERAL(6, 66, 11), // "productName"
QT_MOC_LITERAL(7, 78, 16), // "shortProductName"
QT_MOC_LITERAL(8, 95, 18), // "shortVersionedName"
QT_MOC_LITERAL(9, 114, 11), // "styleString"
QT_MOC_LITERAL(10, 126, 10), // "StyleEntry"
QT_MOC_LITERAL(11, 137, 10), // "styleEntry"
QT_MOC_LITERAL(12, 148, 9), // "imagePath"
QT_MOC_LITERAL(13, 158, 10), // "ImageEntry"
QT_MOC_LITERAL(14, 169, 10), // "imageEntry"
QT_MOC_LITERAL(15, 180, 11), // "sidebarSide"
QT_MOC_LITERAL(16, 192, 9), // "PanelSide"
QT_MOC_LITERAL(17, 202, 14), // "navigationSide"
QT_MOC_LITERAL(18, 217, 11), // "ProductName"
QT_MOC_LITERAL(19, 229, 7), // "Version"
QT_MOC_LITERAL(20, 237, 12), // "ShortVersion"
QT_MOC_LITERAL(21, 250, 13), // "VersionedName"
QT_MOC_LITERAL(22, 264, 18), // "ShortVersionedName"
QT_MOC_LITERAL(23, 283, 16), // "ShortProductName"
QT_MOC_LITERAL(24, 300, 19), // "BootloaderEntryName"
QT_MOC_LITERAL(25, 320, 10), // "ProductUrl"
QT_MOC_LITERAL(26, 331, 10), // "SupportUrl"
QT_MOC_LITERAL(27, 342, 14), // "KnownIssuesUrl"
QT_MOC_LITERAL(28, 357, 15), // "ReleaseNotesUrl"
QT_MOC_LITERAL(29, 373, 9), // "DonateUrl"
QT_MOC_LITERAL(30, 383, 13), // "ProductBanner"
QT_MOC_LITERAL(31, 397, 11), // "ProductIcon"
QT_MOC_LITERAL(32, 409, 11), // "ProductLogo"
QT_MOC_LITERAL(33, 421, 16), // "ProductWallpaper"
QT_MOC_LITERAL(34, 438, 14), // "ProductWelcome"
QT_MOC_LITERAL(35, 453, 17), // "SidebarBackground"
QT_MOC_LITERAL(36, 471, 11), // "SidebarText"
QT_MOC_LITERAL(37, 483, 17), // "SidebarTextSelect"
QT_MOC_LITERAL(38, 501, 19), // "SidebarTextSelected"
QT_MOC_LITERAL(39, 521, 20), // "SidebarTextHighlight"
QT_MOC_LITERAL(40, 542, 25), // "SidebarBackgroundSelected"
QT_MOC_LITERAL(41, 568, 15), // "WindowExpansion"
QT_MOC_LITERAL(42, 584, 6), // "Normal"
QT_MOC_LITERAL(43, 591, 10), // "Fullscreen"
QT_MOC_LITERAL(44, 602, 5), // "Fixed"
QT_MOC_LITERAL(45, 608, 19), // "WindowDimensionUnit"
QT_MOC_LITERAL(46, 628, 4), // "None"
QT_MOC_LITERAL(47, 633, 6), // "Pixies"
QT_MOC_LITERAL(48, 640, 7), // "Fonties"
QT_MOC_LITERAL(49, 648, 15), // "WindowPlacement"
QT_MOC_LITERAL(50, 664, 6), // "Center"
QT_MOC_LITERAL(51, 671, 4), // "Free"
QT_MOC_LITERAL(52, 676, 11), // "PanelFlavor"
QT_MOC_LITERAL(53, 688, 6), // "Widget"
QT_MOC_LITERAL(54, 695, 3), // "Qml"
QT_MOC_LITERAL(55, 699, 4), // "Left"
QT_MOC_LITERAL(56, 704, 5), // "Right"
QT_MOC_LITERAL(57, 710, 3), // "Top"
QT_MOC_LITERAL(58, 714, 6) // "Bottom"

    },
    "Calamares::Branding\0string\0\0StringEntry\0"
    "stringEntry\0versionedName\0productName\0"
    "shortProductName\0shortVersionedName\0"
    "styleString\0StyleEntry\0styleEntry\0"
    "imagePath\0ImageEntry\0imageEntry\0"
    "sidebarSide\0PanelSide\0navigationSide\0"
    "ProductName\0Version\0ShortVersion\0"
    "VersionedName\0ShortVersionedName\0"
    "ShortProductName\0BootloaderEntryName\0"
    "ProductUrl\0SupportUrl\0KnownIssuesUrl\0"
    "ReleaseNotesUrl\0DonateUrl\0ProductBanner\0"
    "ProductIcon\0ProductLogo\0ProductWallpaper\0"
    "ProductWelcome\0SidebarBackground\0"
    "SidebarText\0SidebarTextSelect\0"
    "SidebarTextSelected\0SidebarTextHighlight\0"
    "SidebarBackgroundSelected\0WindowExpansion\0"
    "Normal\0Fullscreen\0Fixed\0WindowDimensionUnit\0"
    "None\0Pixies\0Fonties\0WindowPlacement\0"
    "Center\0Free\0PanelFlavor\0Widget\0Qml\0"
    "Left\0Right\0Top\0Bottom"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Calamares__Branding[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       8,   74, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   59,    2, 0x0a /* Public */,
       5,    0,   62,    2, 0x0a /* Public */,
       6,    0,   63,    2, 0x0a /* Public */,
       7,    0,   64,    2, 0x0a /* Public */,
       8,    0,   65,    2, 0x0a /* Public */,
       9,    1,   66,    2, 0x0a /* Public */,
      12,    1,   69,    2, 0x0a /* Public */,
      15,    0,   72,    2, 0x0a /* Public */,
      17,    0,   73,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::QString, 0x80000000 | 3,    4,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString,
    QMetaType::QString, 0x80000000 | 10,   11,
    QMetaType::QString, 0x80000000 | 13,   14,
    0x80000000 | 16,
    0x80000000 | 16,

 // enums: name, alias, flags, count, data
       3,    3, 0x0,   12,  114,
      13,   13, 0x0,    5,  138,
      10,   10, 0x0,    6,  148,
      41,   41, 0x2,    3,  160,
      45,   45, 0x2,    3,  166,
      49,   49, 0x2,    2,  172,
      52,   52, 0x2,    3,  176,
      16,   16, 0x2,    5,  182,

 // enum data: key, value
      18, uint(Calamares::Branding::ProductName),
      19, uint(Calamares::Branding::Version),
      20, uint(Calamares::Branding::ShortVersion),
      21, uint(Calamares::Branding::VersionedName),
      22, uint(Calamares::Branding::ShortVersionedName),
      23, uint(Calamares::Branding::ShortProductName),
      24, uint(Calamares::Branding::BootloaderEntryName),
      25, uint(Calamares::Branding::ProductUrl),
      26, uint(Calamares::Branding::SupportUrl),
      27, uint(Calamares::Branding::KnownIssuesUrl),
      28, uint(Calamares::Branding::ReleaseNotesUrl),
      29, uint(Calamares::Branding::DonateUrl),
      30, uint(Calamares::Branding::ProductBanner),
      31, uint(Calamares::Branding::ProductIcon),
      32, uint(Calamares::Branding::ProductLogo),
      33, uint(Calamares::Branding::ProductWallpaper),
      34, uint(Calamares::Branding::ProductWelcome),
      35, uint(Calamares::Branding::SidebarBackground),
      36, uint(Calamares::Branding::SidebarText),
      37, uint(Calamares::Branding::SidebarTextSelect),
      38, uint(Calamares::Branding::SidebarTextSelected),
      39, uint(Calamares::Branding::SidebarTextHighlight),
      40, uint(Calamares::Branding::SidebarBackgroundSelected),
      42, uint(Calamares::Branding::WindowExpansion::Normal),
      43, uint(Calamares::Branding::WindowExpansion::Fullscreen),
      44, uint(Calamares::Branding::WindowExpansion::Fixed),
      46, uint(Calamares::Branding::WindowDimensionUnit::None),
      47, uint(Calamares::Branding::WindowDimensionUnit::Pixies),
      48, uint(Calamares::Branding::WindowDimensionUnit::Fonties),
      50, uint(Calamares::Branding::WindowPlacement::Center),
      51, uint(Calamares::Branding::WindowPlacement::Free),
      46, uint(Calamares::Branding::PanelFlavor::None),
      53, uint(Calamares::Branding::PanelFlavor::Widget),
      54, uint(Calamares::Branding::PanelFlavor::Qml),
      46, uint(Calamares::Branding::PanelSide::None),
      55, uint(Calamares::Branding::PanelSide::Left),
      56, uint(Calamares::Branding::PanelSide::Right),
      57, uint(Calamares::Branding::PanelSide::Top),
      58, uint(Calamares::Branding::PanelSide::Bottom),

       0        // eod
};

void Calamares::Branding::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<Branding *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: { QString _r = _t->string((*reinterpret_cast< StringEntry(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 1: { QString _r = _t->versionedName();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 2: { QString _r = _t->productName();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 3: { QString _r = _t->shortProductName();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 4: { QString _r = _t->shortVersionedName();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 5: { QString _r = _t->styleString((*reinterpret_cast< StyleEntry(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 6: { QString _r = _t->imagePath((*reinterpret_cast< ImageEntry(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 7: { PanelSide _r = _t->sidebarSide();
            if (_a[0]) *reinterpret_cast< PanelSide*>(_a[0]) = std::move(_r); }  break;
        case 8: { PanelSide _r = _t->navigationSide();
            if (_a[0]) *reinterpret_cast< PanelSide*>(_a[0]) = std::move(_r); }  break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject Calamares::Branding::staticMetaObject = { {
    QMetaObject::SuperData::link<QObject::staticMetaObject>(),
    qt_meta_stringdata_Calamares__Branding.data,
    qt_meta_data_Calamares__Branding,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Calamares::Branding::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Calamares::Branding::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Calamares__Branding.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int Calamares::Branding::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 9)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 9;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
